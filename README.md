
# react-native-plugin

## Getting started

`$ npm install react-native-plugin --save`

### Mostly automatic installation

`$ react-native link react-native-plugin`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-plugin` and add `RNPlugin.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNPlugin.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.qmaple.tapit.plugin.RNPluginPackage;` to the imports at the top of the file
  - Add `new RNPluginPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-plugin'
  	project(':react-native-plugin').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-plugin/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-plugin')
  	```


## Usage
```javascript
import RNPlugin from 'react-native-plugin';

// TODO: What to do with the module?
RNPlugin;
```
  