
package com.qmaple.tapit.plugin;

import android.content.Intent;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.huawei.hms.jos.AppUpdateClient;
import com.huawei.hms.jos.JosApps;
import com.huawei.updatesdk.service.appmgr.bean.ApkUpgradeInfo;
import com.huawei.updatesdk.service.otaupdate.CheckUpdateCallBack;
import com.huawei.updatesdk.service.otaupdate.UpdateKey;

import java.io.Serializable;

public class RNPluginModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public RNPluginModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "RNPlugin";
  }

  @ReactMethod
  public void checkHmsJosUpdate(final Callback onUpdate) {
    class UpdateCallBack implements CheckUpdateCallBack {
      private Callback callback;
      private UpdateCallBack(Callback callback) {
        this.callback = callback;
      }
      public void onUpdateInfo(Intent intent) {
        if (intent != null) {
          // 获取更新状态码， Default_value为取不到status时默认的返回码，由应用自行决定
          int status = intent.getIntExtra(UpdateKey.STATUS, 0);
          // 错误码，建议打印
          int rtnCode = intent.getIntExtra(UpdateKey.FAIL_CODE, 0);
          // 失败信息，建议打印
          String rtnMessage = intent.getStringExtra(UpdateKey.FAIL_REASON);
          Serializable info = intent.getSerializableExtra(UpdateKey.INFO);
          //可通过获取到的info是否属于ApkUpgradeInfo类型来判断应用是否有更新
          if (info instanceof ApkUpgradeInfo) {
            String version = ((ApkUpgradeInfo) info).getVersion_();
            String url = ((ApkUpgradeInfo) info).getDownurl_();
            String features = ((ApkUpgradeInfo) info).getNewFeatures_();
            String infoStr = ((ApkUpgradeInfo) info).toJson();
            final WritableMap infoMap = Arguments.createMap();
            infoMap.putString("version", version);
            infoMap.putString("url", url);
            infoMap.putString("features", features);
            infoMap.putString("json", infoStr);
            this.callback.invoke(infoMap);
            return;
          }
        }
        this.callback.invoke("");
      }

      @Override
      public void onMarketInstallInfo(Intent intent) {
      }

      @Override
      public void onMarketStoreError(int i) {
      }

      @Override
      public void onUpdateStoreError(int i) {
      }
    }

    AppUpdateClient client = JosApps.getAppUpdateClient(this.reactContext);
    client.checkAppUpdate(this.reactContext, new UpdateCallBack(onUpdate));
  }
}