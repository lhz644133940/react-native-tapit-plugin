
import { NativeModules } from 'react-native';

const { RNPlugin } = NativeModules;

export default RNPlugin;
